//
// Created by Home on 11/1/2022.
//
#include <iostream>
#include <vector>
#include "Data_haha.h"
#include <chrono>
using namespace std::chrono;
using namespace std;
const int base = 10;

vector<int> slice(const vector<int> &v, int begin, int end) {//включно
    vector<int> sliced;
    if(begin>end) {
        sliced.push_back(0);
        return sliced;
    }
    for(int i=begin; i<end+1; i++){
        sliced.push_back(v[i]);
    }
    return sliced;
}

vector<int>& throw_some_zeros(vector<int> &v, int k){
    for(int i=0; i<k; i++){
        v.push_back(0);
    }
    return v;
}

LongInt Naive::multiply(LongInt a, LongInt b) {
    int length = a.get_len()+b.get_len()-1;
    vector<int> product(length, 0);
    for (int i = 0; i < a.get_len(); i++)
    {
        for (int j = 0; j < b.get_len(); j++)
        {
            product[length - 1 - i - j] += a[i] * b[j];
        }
    }
    product.push_back(0);
    for (int i = 0; i < length; i++)
    {
        product[i + 1] += product[i] / base;
        product[i] %= base;
    }
    int sign = 0;
    if (a.sign() != b.sign()) sign = -1; // обробка знаку
    else sign = 1;
    return LongInt(sign)*LongInt(product);
}

LongInt Karatsuba::multiply(LongInt a, LongInt b){
    if(a==LongInt(0)) return {0};
    if(b==LongInt(0)) return {0};
    Karatsuba obj;

    if (a.get_len() < Kara_min_length and b.get_len() < Kara_min_length) {
        Naive obj1;
        return obj1.multiply(a, b);
    }

    vector<int> Va = a.digits_backwards;
    vector<int> Vb = b.digits_backwards;
    if(a.get_len()<b.get_len()) {throw_some_zeros(Va, b.get_len()-a.get_len());}
    else if(a.get_len()>b.get_len()) {throw_some_zeros(Vb, a.get_len()-b.get_len());}

    int n = Va.size();
    int k = (n+1)/2;


    LongInt A0 = slice(Va, 0, k-1);
    LongInt A1 = slice(Va, k, n-1);
    LongInt B0 = slice(Vb, 0, k-1);
    LongInt B1 = slice(Vb, k, n-1);

    LongInt C0 = obj.multiply(A0, B0);
    LongInt C1 = obj.multiply(A1, B1);
    LongInt C2 = obj.multiply((A0-A1).abs(), (B0-B1).abs());
    LongInt SA = (A0-A1).sign();
    LongInt SB = (B0-B1).sign();
    return C0 +  (C0+C1-SA*SB*C2).mult_by_base(k) + C1.mult_by_base(2*k);
}

LongInt Toom3::multiply(LongInt a, LongInt b){

    Toom3 toom;
    LongInt P;
    if(a==LongInt(0) or b==LongInt(0)) {return {0};}

    if (a.get_len() < Toom3_min_length and b.get_len() < Toom3_min_length) {
        Naive obj1;
        return obj1.multiply(a, b);
    }

    vector<int> Va = a.digits_backwards;
    vector<int> Vb = b.digits_backwards;
    if(a.get_len()<b.get_len()) {throw_some_zeros(Va, b.get_len()-a.get_len());}
    else if(a.get_len()>b.get_len()) {throw_some_zeros(Vb, a.get_len()-b.get_len());}

    int n = Va.size();
    int k = (n+2)/3;

    LongInt A0 = slice(Va, 0, k-1);
    LongInt A1 = slice(Va, k, 2*k-1);
    LongInt A2 = slice(Va, 2*k, n-1);
    LongInt B0 = slice(Vb, 0, k-1);
    LongInt B1 = slice(Vb, k, 2*k-1);
    LongInt B2 = slice(Vb, 2*k, n-1);
    LongInt A02 = A0+A2, B02 = B0+B2;

    LongInt V0 = toom.multiply(A0, B0);
    LongInt V1 = toom.multiply(A02+A1, B02+B1);
    LongInt V_1 = toom.multiply(A02-A1, B02-B1);
    LongInt V2 = toom.multiply(A0+LongInt(2)*A1+LongInt(4)*A2, B0+LongInt(2)*B1+LongInt(4)*B2);
    LongInt V_inf = toom.multiply(A2, B2);
    // на цьому кроці вже треба реялізувати ділення націло
    LongInt T1 = (V0+V0+V0+V_1+V_1+V2)/6 - (V_inf+V_inf);
    LongInt T2 = (V1+V_1)/2;
    LongInt C0 = V0;
    LongInt C1 = V1-T1;
    LongInt C2 = T2-V0-V_inf;
    LongInt C3 = T1-T2;
    LongInt C4 = V_inf;
    P = C0+C1.mult_by_base(k)+C2.mult_by_base(2*k)+C3.mult_by_base(3*k)+C4.mult_by_base(4*k);
    if (a.sign() != b.sign()) {P.positive = false;}
    return P;
}

LongInt Modular::multiply(LongInt a, LongInt b){
    if(a.get_len()+b.get_len()>Modular_max_length){
        //To optimize multiplying smaller numbers Modular multiplication doesn't accept numbers with total length > 100
        Karatsuba obj1;
        return obj1.multiply(a, b);
    }
    LongInt P;
    if(a==LongInt(0) or b==LongInt(0)) {return {0};}
    if (a.get_len() < Modular_min_length or b.get_len() < Modular_min_length) {
        Karatsuba obj1;
        return obj1.multiply(a, b);
    }

    LongInt residues_of_a[25];
    for(int i=0; i<25; i++){
        residues_of_a[i]=a%m[i];
    }
    LongInt residues_of_b[25];
    for(int i=0; i<25; i++){
        residues_of_b[i]=b%m[i];
    }

    LongInt residues_of_c[25];
    for(int i=0; i<25; i++){
        residues_of_c[i]=(residues_of_a[i]*residues_of_b[i])%m[i];
    }

    LongInt c=0;
    for(int i=0; i<25; i++){
        c = c+residues_of_c[i]*final_products[i];
    }
    return c%M;
}

void testing_multiplication_time(LongInt &A, LongInt &B){

    LongInt::SetMult(new Karatsuba);
    auto start = high_resolution_clock::now();
    LongInt Test = A*B;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    cout << Test.get_len() << " " << Test << "\n" << duration.count() <<" milliseconds for Karatsuba Multiplication" <<endl;

    LongInt::SetMult(new Toom3);
    auto start2 = high_resolution_clock::now();
    LongInt Test2 = A*B;
    auto stop2 = high_resolution_clock::now();
    auto duration2 = duration_cast<milliseconds>(stop2 - start2);
    cout << Test2.get_len() << " " << Test2 << "\n" << duration2.count() <<" milliseconds for Toom3 multiplication" <<endl;

    LongInt::SetMult(new Modular);
    auto start3 = high_resolution_clock::now();
    LongInt Test3 = A*B;
    auto stop3 = high_resolution_clock::now();
    auto duration3 = duration_cast<milliseconds>(stop3 - start3);
    cout << Test3.get_len() << " " << Test3 << "\n" << duration3.count() <<" milliseconds for Modular multiplication" <<endl;
}

void testing_Kara_and_Toom(long long begin, long long end){
    bool test = true;
    const long long num_of_checks = (end-begin)*(end-begin);
    Karatsuba kara;
    Toom3 toom;

    for(long long i=begin; i<end; i++ ){
        for(long long j=begin; j<end; j++){
            if(toom.multiply(i, j)==kara.multiply(i, j)){}
            else {
                cout << "AHA GOTCHA! numbers:" << i << " " << j << endl;
                cout << "Products:    Toom: "<<toom.multiply(i, j)<<";    Kara:"<<kara.multiply(i, j);
                cout <<";   real product:"<<i*j<<endl;
                test = false;
            }
        }
    }
    if(test){
        cout << "All " << num_of_checks << " tests passed."<<endl;
    }
    cout << "Testing Karatsuba vs Toom3 finished"<<endl;
}

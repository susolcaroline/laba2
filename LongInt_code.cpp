//
// Created by Home on 10/17/2022.
//
#include <iostream>
#include <vector>
#include "all_classes.h"
#include <algorithm>
#include <cmath>

using namespace std;
const int base=10;
Toom3 obj;
Multiplication *LongInt::mult_type = &obj;

LongInt::LongInt() {
    digits_backwards.push_back(0);
    positive = true;
}

LongInt::LongInt(long long a) {
    if (a==0) {digits_backwards.push_back(0);}
    if (a<0) {positive= false; a=-a;}
    while (a > 0) {
        int digit = a % base;
        digits_backwards.push_back(digit);
        a /= 10;
    }
}

LongInt::LongInt(const vector<int> &v) { // it accepts a vector of backwards digits
    vector<int> v2(v);
    digits_backwards = v2;
    for(int i=v2.size()-1; i>=0; i--){
        if(v2[i]!=0) break;
        else if(i!=0) digits_backwards.pop_back();
    }
    positive = true;
}

LongInt::LongInt(string s) {
    int edge=0;
    if (s[0]=='-') {positive= false; edge=1;}
    for(int i=s.length()-1; i>=edge; i--) { //should work fine
        int digit=s[i]-'0';
        digits_backwards.push_back(digit);
    }
}

void LongInt::print() const{
    if(!positive) cout << "-";
    for(int i=get_len()-1; i>=0; i--){
        cout << digits_backwards[i];
    }
}

ostream& operator<<(ostream& os, const LongInt& val)
{
    os << "";
    (val).print();
    return os;
}


int LongInt::operator[](int index) const{
    if(index<0 or index>=get_len()){
        cout << "Ouch! The index is out of bound " << get_len();
        exit(0);
    }
    index = get_len() - 1 - index;
    return digits_backwards[index];
}

bool LongInt::operator<(const LongInt &b) const {
    if(!positive and b.positive) return true;
    if(positive and !b.positive) return false;
    bool ans;
    if(get_len() < b.get_len())  ans= true;
    else if(get_len() > b.get_len()) ans= false;
    else{
        for(int i=get_len()-1; i>=0; i--){
            if(digits_backwards[i] < b.digits_backwards[i]) {
                ans= true;
                break;
            }
            if(digits_backwards[i] > b.digits_backwards[i]){
                ans= false;
                break;
            }
            if (i==0) return false; //they are equal
        }
    }
    if(positive) return ans;
    else return !ans;
}

bool LongInt::operator>(const LongInt &b) const {
    return b<(*this);
}

bool LongInt::operator==(const LongInt& b) const {
    if (b.positive!=positive) return false;
    if (b.digits_backwards==digits_backwards) return true;
    else return false;
}

LongInt LongInt::operator-() const {
    LongInt ans = *this;
    if(ans.positive) ans.positive = false;
    else ans.positive = true;
    if(*this==LongInt(0)) ans.positive=true;
    return ans;
}

LongInt LongInt::operator-(const LongInt &b) const {

    LongInt term1 = *this;
    LongInt term2 = b;

    if(term1.positive and term2.positive) {
        if (term1 < term2) {
            return -(term2-term1);
        }

        int borrow = 0;
        vector<int> subtrVector;
        for(int i=0; i<term1.get_len(); i++){
            int el1, el2;
            if(i>=b.get_len()){
                el2 = 0;
            }
            else {
                el2 = term2.digits_backwards[i];
            }
            el1 = term1.digits_backwards[i];
            int el;
            el = el1 - el2 - borrow;
            if (el < 0) {borrow = 1; el+=base;}
            else borrow = 0;
            subtrVector.push_back(el);
        }
        return {subtrVector};
    }
    if(term1.positive and not term2.positive){
        return term1+(-term2);
    }
    if(not term1.positive and term2.positive){
        return -((-term1)+term2);
    }
    if(not term1.positive and not term2.positive){
        return -((-term1)-(-term2));
    }
}

LongInt LongInt::operator+(const LongInt& b) const {

    LongInt term1 = *this;
    LongInt term2 = b;

    if(term1.positive and term2.positive){
        if(term1<term2) {
            return term2+term1;
        }
        vector<int> sumVector;
        int transfer=0;
        int range=term1.get_len();
        for(int i=0; i<range; i++){
            int el1, el2;
            if(i>=b.get_len()){
                el2 = 0;
            }
            else {
                el2 = term2.digits_backwards[i];
            }
            el1 = term1.digits_backwards[i];
            int el = (el1+el2+transfer)%base;
            transfer = (el1+el2+transfer)/10;
            sumVector.push_back(el);
        }
        if (transfer==1) sumVector.push_back(1);
        return {sumVector}; // це не я, це clang-tidy
    }
    else if(term1.positive and !term2.positive){
        term2.positive = true;
        return term1 - term2;
    }
    else if(!term1.positive and term2.positive){
        term1.positive = true;
        return term2 - term1;
    }
    else{
        term1.positive = true;
        term2.positive = true;
        LongInt answer = term1+term2;
        answer.positive = false;
        return answer;
    }
}

LongInt LongInt::operator*(const LongInt& b) const {
    if ((*this)==LongInt(0) or b==LongInt(0)) return {0};
    if ((*this)==LongInt(1)) return b;
    if(b==LongInt(1)) return (*this);

    if(!(*this).positive and !b.positive) return (-*this)*(-b);
    if(!(*this).positive and b.positive or (*this).positive and !b.positive) {return -((*this).abs() * b.abs());}
    return mult_type->multiply(*this, b);
}

LongInt LongInt::operator/(const int& digit) const{
    if(!(*this).positive and digit<0) return (-*this) / (-digit);
    if(!(*this).positive and digit>=0 or (*this).positive and digit<0) return -((*this).abs()/::abs(digit));
    if(digit==0) {
        cout << "YOU MORON DIVIDED BY ZERO";
        exit(0);
    }
    LongInt C=(*this);

    int transfer = 0;
    vector<int> ans;
    for(int i=0; i<C.get_len(); i++){
        int el = C[i]+base*transfer;
        ans.push_back(el/digit);
        transfer = el%digit;
    }
    reverse(ans.begin(), ans.end());
    return {ans};
}

LongInt LongInt::operator%(const LongInt& Divisor) const{
    LongInt Divident = (*this);
    if(Divisor.sign()==-1) {
        cout << "Can't do % operation with negative divisor";
        exit(1);
    }
    if(Divident.sign()==-1){
        LongInt Answer = Divident.abs()%Divisor;
        return Divisor-Answer;
    }

    if(Divident<Divisor) return (*this);
    if(Divisor==LongInt(0)) {
        cout << "Oh man";
        exit(1);
    }

    if(Divident.get_len()>Divisor.get_len()+1){
        int m = Divident.get_len() - Divisor.get_len();
        Divident = Divident%Divisor.mult_by_base(m-1);
        return Divident%Divisor;
    }
    else while(Divident>Divisor or Divident==Divisor){
            Divident = Divident - Divisor;
        }
    return Divident;
}

LongInt LongInt::abs() const {
    LongInt ans = (*this);
    ans.positive = true;
    return ans;
}

int LongInt::sign()const {
    if(positive) return 1;
    else return -1;
}

LongInt LongInt::mult_by_base(int k) const{
    vector<int> ans=(*this).digits_backwards;
    reverse(ans.begin(), ans.end());
    for(int i=0; i<k; i++){
        ans.push_back(0);
    }
    reverse(ans.begin(), ans.end());
    return {ans};
}

int LongInt::get_len() const {
    return (*this).digits_backwards.size();
}

LongInt random(LongInt begin, LongInt end){ //end is not included
    if(begin>end or begin==end) {
        cout<<"Sorry, the range is unacceptable";
        exit(0);
    }
    LongInt diff = begin - end;
    // now I am going to make an illegal move
    int k=rand();
    if(diff > LongInt(RAND_MAX)) {
        return begin+LongInt(k);
    }
    else{
        int m = diff.to_int();
        return begin+LongInt(rand()%m);
    }
}

int LongInt::to_int() const {
    if(*this<LongInt(2147483647)){
        int result=0;
        for(int i=0; i<get_len(); i++){
            result+=(*this)[i]*pow(base, get_len()-i-1);
        }
        if(not positive) result=-result;
        return result;
    }
    else {
        cout << "Can't convert the number to int";
        exit(0);
    }
}

LongDouble LongInt::inverse() const{
    LongInt D = *this;
    if(D.sign()==-1) D=-D;
    LongDouble x(LongInt(1), -get_len()-1);
    int M = 50;
    for(int i=0; i<M; i++){
        x = (x+x-x*x*LongDouble(D, 0)).cut_precision(LongDouble::PRECISION_for_inverse);
    }
    if(sign()==-1) {
        x = -x;
    }
    return x;
}

//
// Created by Home on 11/10/2022.
//
#include <iostream>
#include <vector>
#include <algorithm>
#include "all_classes.h"


LongDouble::LongDouble(LongInt Num, int ord){
    Number = Num;
    order = ord;
    if(!(Num==LongInt(0))){
        int j=Num.get_len()-1;
        while(Num[j]==0 and j>0){
            order++;
            Number.digits_backwards.erase(Number.digits_backwards.begin());
            j--;
        }
    }
}

LongDouble LongDouble::operator+(const LongDouble& b) const{ //0.9+1.234
    if(order<b.order){
        // доповнюємо число b до порядку this (зменшуємо порядок b)
        LongInt Answer = Number + b.Number.mult_by_base(b.order-order);
        return {Answer, order};
    }
    else{
        LongInt Answer = b.Number + Number.mult_by_base(order-b.order);
        return {Answer, b.order};
    }
}

LongDouble LongDouble::operator-(const LongDouble& b) const{
    if(order<b.order){
        // доповнюємо число b до порядку this (зменшуємо порядок b)
        LongInt Answer = Number - b.Number.mult_by_base(b.order-order);
        return {Answer, order};
    }
    else{
        LongInt Answer = Number.mult_by_base(order-b.order) - b.Number;
        return {Answer, b.order};
    }
}
LongDouble LongDouble::operator-() const{
    return {-Number, order};
}
LongDouble LongDouble::operator*(const LongDouble& b) const{
    return {Number*b.Number, order+b.order};
}

ostream& operator<<(ostream& os, const LongDouble& val){
    os << "";
    if(! val.Number.positive) cout << "-";
    bool in = false;
    int already_printed = 0;
    for(int i=0; i<val.Number.get_len()+val.order; i++){
        cout<<val.Number[i];
        already_printed++;
        in = true;
    }
    if(in){
        cout << ".";
    }
    else{
        cout << "0.";
    }
    for(int i=0; i<-(val.Number.get_len()+val.order); i++){
        cout << "0";
    }
    for(int i=already_printed; i<val.Number.get_len(); i++){
        cout << val.Number[i];
    }
    return os;
}

LongDouble LongDouble::cut_precision(int k) const{
    if(-order>k){// 9.1234567 k=2 order=-7 із 76543219 лишаємо 219
        int end = Number.get_len()-1;
        int begin = end-(k+get_before_dot())+1;
        vector<int> shrink_to_k_digits = slice(Number.digits_backwards, begin, end);
        LongInt Main=shrink_to_k_digits;
        if(!Number.positive) Main.positive=false;
        return {Main, -k};
    }
    else return *this;
}

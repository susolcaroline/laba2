#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include "all_classes.h"
using namespace std;
using  namespace std::chrono;
int main() {
    LongInt::SetMult(new Toom3);

    //LongDouble Double(LongInt(7134), -7);
    //cout <<  Double << endl;

    //testing_primality(&SolovayStrassen_primality, 4, 1004);
    //testing_Kara_and_Toom(-100, 100);

    auto start = high_resolution_clock::now();
    cout << LongInt(151151515555).inverse();
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<seconds>(stop - start);
    cout << duration.count() << endl;

    // Comparing time
    /*LongInt A("-462625005053620650528034420216445846564565465645334");
    LongInt B("70535623574295495455347854153863");
    testing_multiplication_time(A, B);*/
    cout << 1.0/151151515555;
    return 0;
}

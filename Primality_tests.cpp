//LongInt mod_pow(const int a, const int n, const int modulo) const;
// Created by Home on 11/3/2022.
//

#include <iostream>
#include <vector>
#include "all_classes.h"

using namespace std;
const int base=10;

bool is_prime(long long num){
    bool prime=true;
    for (int i = 2; i*i <= num; ++i) {
        if (num % i == 0) {
            prime = false;
            break;
        }
    }
    return prime;
}

LongInt mod_pow(const LongInt& a, LongInt n, const LongInt& modulo) {
    LongInt ans(0);
    if(n==0) {ans=LongInt(1); return ans;}
    if(n%2==0){
        LongInt intermediate = mod_pow(a, n/2, modulo);
        ans = (intermediate*intermediate);
        ans = ans%modulo;
    }
    else{
        LongInt intermediate = mod_pow(a, (n-1)/2, modulo);
        ans = intermediate*intermediate;
        ans = ans%modulo;
        ans = (ans*a)%modulo;
    }
    return ans;
}

pair<int, LongInt> power_of_two(LongInt val){
    int ans = 0;
    while(LongInt(val)>LongInt(0)){
        if(LongInt(2)*(val/2)==val) {val = val/2; ans++;}
        else break;
    }
    pair<int, LongInt> PAIR (ans, val);
    return PAIR;
}

bool Fermat_primality(const LongInt& n, int num_of_checks) {
    int k=1;
    for(int i=2; i<num_of_checks+2; i++){
        if(LongInt(i)==n*k) {k++; continue;}
        LongInt power = n-LongInt(1);
        LongInt num = mod_pow(LongInt(i), power, n);
        if (num==LongInt(1)){
            // probability that n is a prime number increases
        }
        else {
            return false;
        }
    }
    return true;
}

bool RabinMiller_primality(const LongInt& n, int num_of_checks) {
    if(n<LongInt(4)) {
        cout << "the number has to be >3" << endl;
        if (n<2) return false;
        else return true;
    }
    LongInt n_minus_1 = n-LongInt(1);
    pair<int, LongInt> div_by_2 = power_of_two(n_minus_1);
    int s = div_by_2.first;
    LongInt d = div_by_2.second;

    for(int i=0; i<num_of_checks; i++){
        bool flag = false;
        LongInt a = random(LongInt(2), n_minus_1);
        LongInt x = mod_pow(a, d, n);
        if(x==LongInt(1) or x==n_minus_1) {continue;}
        for(int j=0; j<s-1; j++){
            //x = mod_pow(x, LongInt(2), n);
            x = (x*x)%n; // this will probably run faster
            if(x==n_minus_1) {
                flag=true;
                break;
            }
        }
        if(flag) continue;
        else return false;
    }
    //cout<<n<<endl;
    return true;
}

bool SolovayStrassen_primality(const LongInt& n, int num_of_checks){
    LongInt n_minus_1 = n-LongInt(1);
    for(int i=0; i<num_of_checks; i++){
        LongInt a = random(LongInt(2), n);
        LongInt x = mod_pow(a, n_minus_1/2, n);
        if(x==LongInt(0)) return false;
        if(x==LongInt(1) or x==n_minus_1) {
            //everything is fine
        }
        else return false;
    }
    return true;
}


void testing_primality(bool method(const LongInt&, int), int begin, int end){
    bool test = true;
    const int num_of_checks = end-begin;

    for(int i=begin; i<end; i++){
        if(is_prime(i)==method(LongInt(i), 20)){}
        else {
            cout << "AHA! "<<i<<" "<<method(LongInt(i), 20)<<endl;
            test = false;
        }
    }
    if(test) {
        cout << "All " << num_of_checks << " tests passed."<<endl;
    }
    cout << "Testing finished"<< endl;
}

/*bool AKS_primality_test(const LongInt& n){

    return true;
}*/
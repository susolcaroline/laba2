//
// Created by Home on 10/17/2022.
//

#ifndef LAB2_ALL_CLASSES_H
#define LAB2_ALL_CLASSES_H

#endif //LAB2_ALL_CLASSES_H
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
class Multiplication;
class LongDouble;

class LongInt{
private:
    vector <int> digits_backwards;
    static Multiplication *mult_type;
public:
    bool positive = true;
    friend class Multiplication;
    friend class Karatsuba;
    friend class Toom3;
    friend class LongDouble;
    int get_len() const;
    LongInt();
    LongInt(long long a);
    LongInt(string s);
    LongInt(const vector<int> &v);
    void print() const;
    friend ostream& operator<<(ostream& os, const LongInt& val);
    static void SetMult(Multiplication *m_type){
        mult_type = m_type;
    };
    int operator[](int index) const;
    bool operator<(const LongInt& b) const;
    bool operator>(const LongInt& b) const;
    bool operator==(const LongInt& b) const;
    LongInt operator-(const LongInt& b) const;
    LongInt operator-() const;
    LongInt operator+(const LongInt& b) const;
    LongInt operator*(const LongInt& b) const;
    LongInt operator/(const int& digit) const;
    LongInt operator%(const LongInt& divisor) const;
    LongDouble inverse() const;
    LongInt abs() const;
    int sign() const;
    LongInt mult_by_base(int k) const;
    int to_int() const;
};

class LongDouble {
    LongInt Number; // наприклад, для -0.0123 Num=-123
    int order; // для -0.0123 order = -4
public:
    LongDouble(LongInt Num, int ord);
    LongDouble operator-(const LongDouble& b) const;
    LongDouble operator-() const;
    LongDouble operator+(const LongDouble& b) const;
    LongDouble operator*(const LongDouble& b) const;
    friend ostream& operator<<(ostream& os, const LongDouble& val);
    LongDouble cut_precision(int num_of_digits_after_dot) const;
    inline int get_after_dot() const {return -order;}
    inline int get_before_dot() const {return Number.get_len()-get_after_dot();}
    static const int PRECISION_for_inverse = 50;
};

class Multiplication{
public:
    virtual LongInt multiply(LongInt a, LongInt b)=0;

};

class Naive: public Multiplication{
public:
    LongInt multiply(LongInt a, LongInt b);
};

class Karatsuba: public Multiplication{
public:
    const int Kara_min_length = 10;
    LongInt multiply(LongInt a, LongInt b);
};


class Toom3: public Multiplication{
public:
    const int Toom3_min_length = 3;
    LongInt multiply(LongInt a, LongInt b);
};

class Modular: public Multiplication{
public:
    const int Modular_min_length = 6;
    const int Modular_max_length = 100;
    LongInt multiply(LongInt a, LongInt b);
};


void testing_multiplication_time(LongInt A, LongInt B);
void testing_primality(bool method(const LongInt&, int), int begin, int end);
void testing_Kara_and_Toom(long long begin, long long end);


vector<int> slice(const vector<int> &v, int begin, int end);
LongInt mod_pow(const LongInt& a, LongInt n, const LongInt& modulo);
pair<int, LongInt> power_of_two(LongInt val);
LongInt random(LongInt begin, LongInt end);

bool is_prime(long long num);
bool Fermat_primality(const LongInt& n, int num_of_checks); //https://bigprimes.org/ to test big numbers
bool RabinMiller_primality(const LongInt& n, int num_of_checks);
bool SolovayStrassen_primality(const LongInt& n, int num_of_checks);
